\version "2.20.0"
\language "english"
theStaffSize = 18
\include "ajb-common.ly"

%%% Functions to aid parenthesizing notes

#(define-public
  (parentheses-item::calc-parenthesis-left-stencils grob)
  (let* ((font (ly:grob-default-font grob))
         (lp (ly:font-get-glyph font "accidentals.leftparen")))
  (list lp empty-stencil)))

#(define-public
  (parentheses-item::calc-parenthesis-right-stencils grob)
  (let* ((font (ly:grob-default-font grob))
         (rp (ly:font-get-glyph font "accidentals.rightparen")))
  (list empty-stencil rp)))

pLeft = \once \override ParenthesesItem.stencils = #parentheses-item::calc-parenthesis-left-stencils

pRight = \once \override ParenthesesItem.stencils = #parentheses-item::calc-parenthesis-right-stencils


\header {
  title = "Intercessions"
  composer = "Alex Ball"
  copyright = \markup {
  \center-column {
    "© Alex Ball 2020. This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International Licence:"
    "https://creativecommons.org/licenses/by-nc/4.0/"
  } }
  tagline = "v1 – 2020-07-05"
}

\paper {
  #(set-paper-size "a4")
  ragged-last-bottom = ##f
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

globalA = {
  \key c \minor
  \time 4/4
  \tempo 4=100
}

scoreABassVoiceI = \relative c' {
  \globalA
  \dynamicUp
  % Music follows here.
  R1*2
  c2 c4 bf
  g1 ~
  g2. r4 \bar"|."
}

scoreAVerseBassVoiceI = \lyricmode {
  % Lyrics follow here.
  Lord, hear our prayer.
}

scoreABassVoiceII = \relative c' {
  \globalA
  \dynamicUp
  % Music follows here.
  R1
  g2 g4 f
  g2 g2
  g4 f c2 ~
  c2. r4
}

scoreAVerseBassVoiceII = \lyricmode {
  % Lyrics follow here.
  Lord, hear our prayer.
  Lord, hear our prayer.
}

scoreABassVoiceIII = \relative c {
  \globalA
  \dynamicUp
  % Music follows here.
  c2 c4 bf
  c1
  ef2 ef4 d
  c2 c4 bf
  c2. r4
}

scoreAVerseBassVoiceIII = \lyricmode {
  % Lyrics follow here.
  Lord, hear our prayer.
  Lord, hear our prayer,
  hear our prayer.
}

scoreABassVoiceIPart = \new Staff \with {
  instrumentName = "T."
  midiInstrument = "choir aahs"
} { \clef bass \scoreABassVoiceI }
\addlyrics { \scoreAVerseBassVoiceI }

scoreABassVoiceIIPart = \new Staff \with {
  instrumentName = "Ba."
  midiInstrument = "choir aahs"
} { \clef bass \scoreABassVoiceII }
\addlyrics { \scoreAVerseBassVoiceII }

scoreABassVoiceIIIPart = \new Staff \with {
  instrumentName = "B."
  midiInstrument = "choir aahs"
} { \clef bass \scoreABassVoiceIII }
\addlyrics { \scoreAVerseBassVoiceIII }

\markup{
  \bold{
    Before each petition:
  }
}

\score {
  \new ChoirStaff <<
    \scoreABassVoiceIPart
    \scoreABassVoiceIIPart
    \scoreABassVoiceIIIPart
  >>
  \layout { }
  \midi { }
}


scoreBBassVoiceI = \relative c' {
  \key c \minor
  \dynamicUp
  % Music follows here.
  c\breve ~
  c\breve ~
  c\breve ~
  c2\bar"|."
}

scoreBVerseBassVoiceI = \lyricmode {
  % Lyrics follow here.
  (hum) __
}

scoreBBassVoiceII = \relative c' {
  \key c \minor
  \dynamicUp
  % Music follows here.
  g2 f4 af g \breathe
  f2 ef4 f g \breathe
  \pLeft\parenthesize g2 af4 \pRight\parenthesize g g2 af4 g f g \breathe
  f2 ef4 d c2
}

scoreBVerseBassVoiceII = \lyricmode {
  % Lyrics follow here.
  "O Lord…"
}

scoreBBassVoiceIII = \relative c {
  \key c \minor
  \dynamicUp
  % Music follows here.
  c\breve ~
  c\breve ~
  c\breve ~
  c2
}

scoreBVerseBassVoiceIII = \lyricmode {
  % Lyrics follow here.
  (hum) __
}

scoreBBassVoiceIPart = \new Staff \with {
  instrumentName = "T."
  midiInstrument = "voice oohs"
} { \clef bass \scoreBBassVoiceI }
\addlyrics { \scoreBVerseBassVoiceI }

scoreBBassVoiceIIPart = \new Staff \with {
  instrumentName = "Ba."
  midiInstrument = "choir aahs"
} { \clef bass \scoreBBassVoiceII }
\addlyrics { \scoreBVerseBassVoiceII }

scoreBBassVoiceIIIPart = \new Staff \with {
  instrumentName = "B."
  midiInstrument = "voice oohs"
} { \clef bass \scoreBBassVoiceIII }
\addlyrics { \scoreBVerseBassVoiceIII }

\markup{
  \bold{
    Improvised for each petition (example):
  }
}

\score {
  \new ChoirStaff <<
    \scoreBBassVoiceIPart
    \scoreBBassVoiceIIPart
    \scoreBBassVoiceIIIPart
  >>
  \layout {
    ragged-last = ##f
    \context {
      \Score
      timing = ##f
    }
    \context {
      \Staff
      \remove "Time_signature_engraver"
    }
    \context {
      \Voice
      \remove "Stem_engraver"
    }
  }
  \midi {
    \tempo 4 = 100
  }
}


globalC = {
  \key c \minor
  \time 4/4
  \tempo 4=80
}

scoreCBassVoiceI = \relative c' {
  \globalC
  \dynamicUp
  % Music follows here.
  R1*2
  c4 c8 c c4( bf)
  g2 r4 g4
  af bf g g8 c
  ef4 d8 ef c4 g
  \override TextSpanner.bound-details.left.text = "rall."
  af8(\startTextSpan bf) c4 ef8( f) d4\stopTextSpan
  c2. r4 \bar"|."
}

scoreCVerseBassVoiceI = \lyricmode {
  % Lyrics follow here.
  Mer -- ci -- ful Fa -- ther,
  ac -- cept these prayers for the sake of your Son,
  our sav -- iour, Je -- sus Christ. A -- men.
}

scoreCBassVoiceII = \relative c' {
  \globalC
  \dynamicUp
  % Music follows here.
  R1
  g4 g8 g g4( f)
  g2. g4
  ef4 f g2
  r4 g4 g8 f ef d
  c8 g' c( bf) g4 ef
  f4 g af8( c,) f4
  e2. r4
}

scoreCVerseBassVoiceII = \lyricmode {
  % Lyrics follow here.
  Mer -- ci -- ful Fa -- ther,
  ac -- cept these prayers,
  ac -- cept them for the sake of your Son,
  our sav -- iour, Je -- sus Christ. A -- men.
}

scoreCBassVoiceIII = \relative c {
  \globalC
  \dynamicUp
  % Music follows here.
  c4 c8 c8 c4( bf)
  c1
  c4 d8 ef d4( ef)
  c2 r4 c4
  c4 bf c c8 bf
  af4 af8 bf ef( d) c( bf)
  af4 c af bf
  c2. r4
}

scoreCVerseBassVoiceIII = \lyricmode {
  % Lyrics follow here.
  Mer -- ci -- ful Fa -- ther,
  Mer -- ci -- ful Fa -- ther,
  ac -- cept these prayers for the sake of your Son,
  our sav -- iour, Je -- sus Christ. A -- men.
}

scoreCBassVoiceIPart = \new Staff \with {
  instrumentName = "T."
  midiInstrument = "choir aahs"
} { \clef bass \scoreCBassVoiceI }
\addlyrics { \scoreCVerseBassVoiceI }

scoreCBassVoiceIIPart = \new Staff \with {
  instrumentName = "Ba."
  midiInstrument = "choir aahs"
} { \clef bass \scoreCBassVoiceII }
\addlyrics { \scoreCVerseBassVoiceII }

scoreCBassVoiceIIIPart = \new Staff \with {
  instrumentName = "B."
  midiInstrument = "choir aahs"
} { \clef bass \scoreCBassVoiceIII }
\addlyrics { \scoreCVerseBassVoiceIII }

\markup{
  \bold{
    At the end:
  }
}

\score {
  \new ChoirStaff <<
    \scoreCBassVoiceIPart
    \scoreCBassVoiceIIPart
    \scoreCBassVoiceIIIPart
  >>
  \layout { }
  \midi { }
}

refrainClicks = \new Voice {
  \tempo 4=100
  \set midiInstrument = #"woodblock"
  \repeat unfold 6 \drums {wbh4 wbl wbl wbl} \drums{wbh}
}

petitionClicks = \new Voice {
  \tempo 4=100
  \set midiInstrument = #"woodblock"
  \repeat unfold 12 \drums {wbh4 wbl wbl wbl} \drums{wbh}
}


codaClicks = \new Voice {
  \tempo 4=80
  \set midiInstrument = #"woodblock"
  \drums {
    \repeat unfold 7 { wbh4 wbl wbl wbl }
    \tempo 4=76 wbh4
    \tempo 4=72 wbl
    \tempo 4=68 wbl
    \tempo 4=64 wbl
    wbh4 wbl wbl wbl
  }
}

\book {
  \bookOutputName "refrain-tenor-click"
  \score {
    <<
      \new Staff \with {
        midiInstrument = "choir aahs"
      } {
        R1 \scoreABassVoiceI
      }
      \refrainClicks
    >>
    \midi { }
  }
}

\book {
  \bookOutputName "refrain-baritone-click"
  \score {
    <<
      \new Staff \with {
        midiInstrument = "choir aahs"
      } {
        R1 \scoreABassVoiceII
      }
      \refrainClicks
    >>
    \midi { }
  }
}

\book {
  \bookOutputName "refrain-bass-click"
  \score {
    <<
      \new Staff \with {
        midiInstrument = "choir aahs"
      } {
        R1 \scoreABassVoiceIII
      }
      \refrainClicks
    >>
    \midi { }
  }
}

\book {
  \bookOutputName "coda-tenor-click"
  \score {
    <<
      \new Staff \with {
        midiInstrument = "choir aahs"
      } {
        R1 \scoreCBassVoiceI
      }
      \codaClicks
    >>
    \midi { }
  }
}

\book {
  \bookOutputName "coda-baritone-click"
  \score {
    <<
      \new Staff \with {
        midiInstrument = "choir aahs"
      } {
        R1 \scoreCBassVoiceII
      }
      \codaClicks
    >>
    \midi { }
  }
}

\book {
  \bookOutputName "coda-bass-click"
  \score {
    <<
      \new Staff \with {
        midiInstrument = "choir aahs"
      } {
        R1 \scoreCBassVoiceIII
      }
      \codaClicks
    >>
    \midi { }
  }
}

\book {
  \bookOutputName "hum-tenor-click"
  \score {
    <<
      \new Staff \with {
        midiInstrument = "choir aahs"
      } {
        R1 \relative c' { c1 ~ c ~ c ~ c ~ c }
      }
      \refrainClicks
    >>
    \midi { }
  }
}

\book {
  \bookOutputName "hum-bass-click"
  \score {
    <<
      \new Staff \with {
        midiInstrument = "choir aahs"
      } {
        R1 \relative c { c1 ~ c ~ c ~ c ~ c }
      }
      \refrainClicks
    >>
    \midi { }
  }
}

\book {
  \bookOutputName "hum-backing-click"
  \score {
    <<
      \new Staff \with {
        midiInstrument = "choir aahs"
      } {
        \tempo 4=80
        \relative c {
          \repeat unfold 2 { <c c'>1 ~ q ~ q ~ q ~ q ~ q ~ q ~ q }
        }
      }
    >>
    \midi { }
  }
}
