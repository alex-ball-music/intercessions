# Intercessions

This is a simple and flexible way of singing intercessions, written for 3-part
close harmony. The idea is that you alternate between a metrical setting of
‘Lord, hear our prayer’ and a chanted petition with accompanying drone. The
intercessions conclude with a setting of the ‘Merciful Father’ prayer from
modern Anglican liturgy.

It is in a minor key and sounds a little bit Gregorian or Russian Orthodox.

I first used it in a set of intercessions recorded for a service on 5 July
2020.

## Summary information

  - *Voicing:* Tenor, Baritone, Bass (though could be transposed to suit other
    trios)

  - *Notes on ambitus:* Bass goes down to A-flat. Tenor goes up to F.

  - *Instrumentation:* A capella

  - *Licence:* Creative Commons Attribution-NonCommercial 4.0 International
     Public Licence: <https://creativecommons.org/licenses/by-nc/4.0/>

## Files

This repository contains the [Lilypond](http://lilypond.org) source code. To
compile it yourself, you will also need my [house style
files](https://gitlab.com/alex-ball-music/ajb-lilypond).

For PDF and MIDI downloads, see the [Releases
page](https://gitlab.com/alex-ball-music/intercessions/-/releases).
